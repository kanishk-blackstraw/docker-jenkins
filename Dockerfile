FROM node:10-alpine

LABEL maintainer="kanishk.bansal@blackstraw.ai"

WORKDIR /usr/src/app

COPY package.json .

RUN npm install --production

COPY . .

EXPOSE 8000

CMD node app.js