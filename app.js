const express = require('express');

const port = process.env.port || 8000;

const app = express();

app.get('/', (req, res, next) => {
    res.json('Docker and Jenkins pipeline is working fine and running node server.');
});

app.listen(port);